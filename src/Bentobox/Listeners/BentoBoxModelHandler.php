<?php

namespace Skyrkt\Bentobox\Listeners;

use Illuminate\Http\Request;
use Skyrkt\Bentobox\Services\Inferrer;

abstract class BentoboxModelHandler
{
	/**
	 * Request data
	 *
	 * @var Request
	 */
	protected $request;

	/**
	 * Eloquent model
	 *
	 * @var object
	 */
	protected $model;

	/**
	 * Attribute settings provided in config file
	 * @var array
	 */
	protected $attributes;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Organize the configuration of our Bentobox Model and
	 * fire any actions
	 *
	 * @param  $event
	 * @return boolean
	 */
	public function handle($string, $model)
	{
		if (is_array($model)) {
			$this->model = $model[0];
		} else {
			$this->model = $model;
		}

		if ($this->shouldHandle())
		{
			$this->attributes = Inferrer::attributeConfig($this->model);
			return $this->fire();
		}
	}

	/**
	 * Determine if Bentobox is in manual_inclusion_mode and whether
	 * this model should have Bentobox functions run against it.
	 *
	 * @return boolean
	 */
	private function shouldHandle()
	{
		return Inferrer::shouldIncludeModel($this->model);
	}
}
