<?php

namespace Skyrkt\Bentobox\Listeners;

use Skyrkt\Bentobox\Listeners\BentoboxModelHandler;
use Skyrkt\Bentobox\Models\BentoboxMedia;
use Skyrkt\Bentobox\Services\ImageService;
use Storage;

class StoreModelMedia extends BentoboxModelHandler
{
	/**
	* Bentobox thumbnail preset
	* @var array
	*/
	private $image_original = [
		'width' => null,
		'height' => null
	];

	/**
	 * Bentobox original preset
	 * @var array
	 */
	private $image_thumbnail = [
		'transform_name' => 'bentobox_thumb',
		'width' => '64',
		'height' => '64',
		'constrainUpsize' => false
	];

	/**
	* Fire sequence of tasks required on model event if the
	* model is $bentobox_media_enabled
	*
	* @return boolean
	*/
	protected function fire()
	{
		if (! $this->model->bentobox_media_enabled) return false;
		return $this->getModelMediaAttributes();
	}

	/**
	* Fetch the attributes from the model and attach form request
	* information containing the file, title and alt information
	*
	* @return boolean
	*/
	private function getModelMediaAttributes()
	{
		$media = [];

		foreach($this->attributes as $attribute_name => $attribute) {
			$type = array_get($attribute, 'type');

			if ($this->request->hasFile($attribute_name)) {
				$media[$attribute_name] = $attribute;
				$media[$attribute_name]['attribute'] = $attribute_name;
				$media[$attribute_name]['title'] = $this->
					request->input('bentobox_' . $attribute_name . '_title');
				$media[$attribute_name]['alt'] = $this->
					request->input('bentobox_' . $attribute_name . '_alt');
				$media[$attribute_name]['file'] = $this->
					request->file($attribute_name);
			}
		}

		$media = collect($media);

		if (empty($media)) return false;

		$this->handleMedia($media);
	}

	/**
	 * Handle the various media items
	 *
	 * @param Collection $media
	 * @return void
	 */
	private function handleMedia($media)
	{
		$this->handleImages($media->where('type', 'image'));
	}

	/**
	 * Let's deal w/ those images we got there
	 *
	 * @param  Collection $images
	 * @return void
	 */
	private function handleImages($images)
	{
		$images = $images->map(function ($item) {
			$this->destroyOldMedia($item['attribute']);

			if (empty($item['transformations'])) {
				$defaults = config('bentobox.transformations') ?
					config('bentobox.transformations') : [];

				$item['transformations'] = $defaults;
			}
			array_push($item['transformations'], $this->image_thumbnail);
			array_push($item['transformations'], $this->image_original);

			return $item;
		});

		foreach($images as $image)
		{
			foreach($image['transformations'] as $t)
			{
				$imageService = new ImageService($image['file'], $t);
				$result = $imageService->handle();
				if ($result) {
					$this->storeMedia($result, $image);
				}
			}
		}
	}

	/**
	 * Store our media in default storage container
	 *
	 * @param  object     $media
	 * @param  array|null $info
	 * @return void
	 */
	private function storeMedia($media, array $info = null)
	{
		$fileName = self::setFileName($media['ext'],
										$info['title'],
										$media['transform_name']);

		$directory = config('bentobox.bentobox_media_path') ?
			config('bentobox.bentobox_media_path') : 'bentobox_media';

		$disk = config('bentobox.bentobox_media_disk') ?
			config('bentobox.bentobox_media_disk') : 'public';

		$path = $directory . '/' . $fileName;

		$file = Storage::disk($disk)->put($path, $media['image']->__toString());

		$record = new BentoboxMedia;
		$record->attribute = $info['attribute'];
		$record->transform_name = $media['transform_name'];
		$record->title = $info['title'];
		$record->alt = $info['alt'];
		$record->disk = $disk;
		$record->url = $path;

		$this->model->bentobox_media()->save($record);
	}

	/**
	 * Check if there is any old media records saved on the attribute and destroy
	 * them if they do.
	 *
	 * @return void
	 */
	private function destroyOldMedia($attribute)
	{
		$media = $this->model->bentobox_media()->mediaAttribute($attribute);
		foreach($media as $m)
		{
			if (config('bentobox.destroy_old_media')) {
				Storage::disk($m->disk)->delete($m->url);
			}

			$m->delete();
		}
	}

	/**
	 * Set up filename or make one if we don't have one
	 *
	 * @param string $ext
	 * @param string $name
	 * @param string $mod
	 */
	public static function setFileName($ext = false, $name = null, $mod = null)
	{
		$date = date('Ymdhis');
		$name = $name ? '-' . str_slug($name) : '';
		$mod = $mod ? '-' . str_slug($mod) : '';
		$ext = $ext ? '.' . $ext : '';
		$filename = $date . $name . $mod . $ext;

		return $filename;
	}
}
