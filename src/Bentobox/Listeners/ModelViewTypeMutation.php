<?php

namespace Skyrkt\Bentobox\Listeners;

class ModelViewTypeMutation
{
		/**
		 * Change the inferred models based on their ModelViewType.
		 *
		 * The only example right now is adding a page query
		 * parameter to the urls of Collection type models
		 *
		 * @param  Array  $payload
		 * @return Collection
		 */
		public function handle($payload)
		{
				$inferredModels = $payload->inferredModels;

				$mutatedInferredModels = $inferredModels->map(function($model) {
					if ($model->get('type') === 'Table') {
						$model['urlParams'] = collect([
							'page' => 1,
							'orderBy' => 'created_at',
							'orderDirection' => 'DESC'
					]);
					}
					return $model;
				});

				return $mutatedInferredModels;
		}
}
