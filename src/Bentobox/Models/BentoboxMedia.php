<?php

namespace Skyrkt\Bentobox\Models;
use Storage;

class BentoboxMedia extends BentoboxModel
{
	/**
	* Never apply Bentobox to this model
	*
	* @var boolean
	*/
	public $bentobox_disabled = true;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'bentobox_media';

	/**
	 * Attributes to be automatically casted as Carbon instances
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];

	/**
	 * Eloquent polymorphic relationship to BentoboxMedia enabled models
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function model()
	{
		return $this->morphTo();
	}

	/**
	 * Eloquent Query Scope for fetching specific media items and sizes
	 *
	 * @param  object $query
	 * @param  array $scope
	 * @return object
	 */
	public function scopeMedia($query, $scope)
	{
		if (is_array($scope)) {
			return $query->where('attribute', $scope[0])
				->where('transform_name', $scope[1])->latest()->first();

		} else {
			return $query->where('attribute', $scope)->latest()->first();
		}
	}

	/**
	 * Eloquent Query Scope for fetching specific media items and sizes
	 *
	 * @param  object $query
	 * @param  array $scope
	 * @return object
	 */
	public function scopePath($query, $scope)
	{
		if (is_array($scope)) {
			$res =  $query->where('attribute', $scope[0])
				->where('transform_name', $scope[1])->latest()->first();

		} else {
			$res =  $query->where('attribute', $scope)->latest()->first();
		}

		if ($res === null) {
			return [];

		} else {
			return Storage::disk($this->disk)->url($res->url);
		}
	}

	/**
	 * Scope used to fetch all of the media of a particular attribute name
	 *
	 * @param  object $query
	 * @param  string $scope
	 * @return object
	 */
	public function scopeMediaAttribute($query, $scope)
	{
		return $query->where('attribute', $scope)->latest()->get();
	}
}
