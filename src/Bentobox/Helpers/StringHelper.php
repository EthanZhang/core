<?php

namespace Skyrkt\Bentobox\Helpers;

class StringHelper {
	/**
	 * Helper function to beutify attribute names
	 *
	 * @param  string $string
	 * @return string
	 */
	public static function humanize($string)
	{
		return ucfirst(str_replace('_', ' ', $string));
	}

	/**
	 * Helper function to make a model ref a human readable title
	 *
	 * @param  string $string
	 * @return string
	 */
	public static function humanizeModelRef($string)
	{
		return self::getClassNameParts($string)->implode(' ');
	}

	/**
	 * Helper function to make a model ref a url
	 *
	 * @param  string $string
	 * @return string
	 */
	public static function urlizeModelRef($string)
	{
		$modelWords = self::getClassNameParts($string);
		$modelWords = $modelWords->map(function($word) {
			return lcfirst($word);
		});
		return $modelWords->implode('-');
	}

	/**
	 * When given a full class reference it returns the class name seperated by words in a collection
	 * @param  string $string Full Class Reference
	 * @return Collection         the parts of the class Name
	 */
	private static function getClassNameParts($string)
	{
		$className = ucfirst(explode('\\', $string)[count(explode('\\', $string)) - 1]);
	$classWords = collect(preg_split('/(?=[A-Z])/', $className));
		$classWords->shift();
	return $classWords;
	}
}
