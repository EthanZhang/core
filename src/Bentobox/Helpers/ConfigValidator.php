<?php

namespace Skyrkt\Bentobox\Helpers;
use Skyrkt\Bentobox\Exceptions\BentoConfigurationException;
use Exception;
use FatalThrowableError;

class ConfigValidator {
	public static function validateRootConfig($config)
	{
		ConfigValidator::isArray($config);
	}

	private static function isArray($config, $extraMessage = 'file')
	{
		if (!is_array($config)) {
			throw new BentoConfigurationException("Bentobox configration {$extraMessage} does not return type of Array.");
		}
	}

}
