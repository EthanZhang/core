<?php

namespace Skyrkt\Bentobox\Services\Inferrers;

use File;
use Skyrkt\Bentobox\Helpers\StringHelper;
use Skyrkt\Bentobox\Services\Inferrer;
use Skyrkt\Bentobox\Events\AfterModelInferrance;

class ModelInferrer {

	/**
	* Kicks off the inferrence process to try to look for model files in standard application
	* locations
	*
	* @return Collection the collection of inferred model configurations
	*/
	public function getModelsFromApplication()
	{
		return $this->inferModelsFromFiles();
	}

	/**
	 * Merge user config with default configuration
	 *
	 * @param  Model $model
	 * @return Collection       Model Config
	 */
	public function mergeConfigWithDefault($model)
	{
		return $this->defaultModelConfiguration($model)->merge(
			collect($model->bentobox_config)
		);
	}

	/**
	* Using Laravels File facade, we search in app_path and in app_path/models for any files, look
	* at those files and give the file a default configuration.
	*
	* @return Collection the collection of inferred model configurations
	*/
	private function inferModelsFromFiles()
	{
		$inferredModels = collect([]);
		$files = $this->getAllFilesFromFolders();

		foreach ($files as $file) {
			$modelRef = $this->getNamespaceFromFile($file) . '\\' . $this->getClassNameFromFile($file);
			$model = new $modelRef();
			$modelConfig = $this->mergeConfigWithDefault($model);

			if (Inferrer::shouldIncludeModel($model, $modelConfig)) {
				$inferredModels = $inferredModels->merge(
					[$modelRef => $modelConfig]
				);
			}
		}

		return event(new AfterModelInferrance($inferredModels))[0];
	}
	 /**
		* Using the default paths, as well as custom defined paths, look for files.
		* @return Collection a collection of files.
		*/
	private function getAllFilesFromFolders()
	{
		$files = collect([]);

		$customDirectories = (config('bentobox.model_directories')) ? config('bentobox.model_directories') : [];

		$paths = array_merge([
			app_path(),
			app_path('Models'),
		], $customDirectories);

		foreach ($paths as $path) {
			$files = $files->merge(File::files($path));
		}

		return $files;
	}

	/**
	* Using regex find the line that contains namespace, and pull the namespace from that.
	*
	* @param  string $file File path
	* @return string       The namespace
	*/
	private function getNamespaceFromFile($file)
	{
		$line;
		preg_match('/.*namespace.*/', file_get_contents($file), $line);
		return rtrim(explode(" ", $line[0])[1], ';');
	}

	/**
	* Using regex find the line that contains the class, and pull the name of the class from that
	*
	* @param  string $file file path
	* @return string       the class name
	*/
	private function getClassNameFromFile($file)
	{
		$line;
		preg_match('/.*class.*/', file_get_contents($file), $line);
		return explode(" ", $line[0])[1];
	}

	/**
	* Based on the model ref return the default configuration.
	*
	* @param  Model $model The Model
	* @return array           default configuration
	*/
	private function defaultModelConfiguration($model)
	{
		$modelRef = get_class($model);
		return collect([
			'title' => StringHelper::humanizeModelRef($modelRef),
			'type' => 'Table',
			'url' => StringHelper::urlizeModelRef($modelRef),
			'enabled' => true,
			'disabled' => false,
			'toggle' => false,
			'perPage' => 50
		]);
	}
}
