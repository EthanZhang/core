<?php

namespace Skyrkt\Bentobox\Services;

use Skyrkt\Bentobox\Services\Inferrer;
use Illuminate\Support\Collection;
use Request;

class SidebarService {

	/**
	 * Use to get a collection with nested items for the sidebar
	 *
	 * TODO: Right now we can only merge nested collections one layer deep.
	 * we should really look into making this recursive so we can infinite nesting.
	 *
	 * @return Collection
	 */
	public static function getSideBarLayout()
	{
		$modelSidebarItems = self::getModelSidebarItems();
		$customSidebarItems = self::getCustomSidebarItems();

		$intersectedKeys = $modelSidebarItems->except(
			$modelSidebarItems->diffKeys($customSidebarItems)->keys()->toArray()
		)->keys();

		foreach ($intersectedKeys as $key) {
			$customSidebarItems[$key] = $customSidebarItems[$key]->merge($modelSidebarItems[$key]);
		}

		return $modelSidebarItems->merge($customSidebarItems);
	}

	/**
	 * From the ModelInferrer figure out what models there are, and organize
	 * them into their sections
	 * @return Collection the sections from the model inferrer models
	 */
	private static function getModelSidebarItems()
	{
		$sections = collect();

		$inferredModels = Inferrer::models();

		foreach ($inferredModels as $modelRef => $modelConfig) {
			if ($modelConfig->has('section')) {
				$sectionName = $modelConfig->get('section');
				if (!$sections->has($sectionName)) {
					$sections[$sectionName] = collect([]);
				}
				$sections[$sectionName]->put($modelRef, $modelConfig);
			} else {
				$sections->put($modelRef, $modelConfig);
			}
		}

		return $sections;
	}

	/**
	 * Look at the global config and organize the sidebar items there.
	 * @return Collection nested collection from the global config
	 */
	private static function getCustomSidebarItems()
	{
		return collect(config('bentobox.sidebar'))->map(function($item) {
			return (is_array($item)) ? collect($item) : $item;
		});
	}

	/**
	 * Get the HTML representation of the sidebar.
	 * @return string html
	 */
	public static function printSidebarLayoutAsHtml()
	{
		$layout = self::getSideBarLayout();

		$html = '<ul>';

		function makeItem($layout, $html) {
			foreach ($layout as $name => $value) {
				$html = $html . '<li class="' . SidebarService::isActiveRoute($value) . '">';

				if (is_string($value)) { // Simple URL

					$html = $html . "<a href='$value'>$name</h1>";

				} elseif (is_a($value, Collection::class) && $value->has('url')) { // a model

					$html = $html . "<a href='/" .
						config('bentobox.route_prefix', 'admin') . '/' .
						$value->get('url') . SidebarService::getUrlParams($value) .
						"'>" . $value->get('title') . "</a>";

				} elseif (is_a($value, Collection::class)) { // nested collection
					$html = $html . "<h5 class='sub-title'>$name</h5><ul>";
					$html = makeItem($value, $html);
					$html = $html . "</ul>";
				}

				$html = $html . '</li>';
			}

			return $html;
		}

		$html = makeItem($layout, $html);

		$html = $html . '</ul>';

		return $html;
	}


	/**
	 * Returns whether the current iterating value is active or not
	 *
	 * TODO: this method could be cleaned up.
	 *
	 * @param  string|Collection  $value
	 * @return boolean
	 */
	public static function isActiveRoute($value)
	{
		if (is_string($value)) {
			if (Request::path() === $value) {
				return 'active';
			}
		} elseif (is_a($value, Collection::class) && $value->has('url')) {
			if (Request::path() === config('bentobox.route_prefix', 'admin') . '/' . $value->get('url')) {
				return 'active';
			}
		}
		return '';
	}

	public static function getUrlParams($modelConfig)
	{
		if ($modelConfig->has('urlParams')) {
			$html = "?";
			foreach ($modelConfig->get('urlParams') as $key => $value) {
				$html = $html . $key . '=' . $value . '&';
			}
			return substr($html, 0, -1);
		}
		return '';
	}
}
