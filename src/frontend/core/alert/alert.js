export class Alert {

  constructor(element) {
    this.element = element;
    this.element.querySelector('.close').addEventListener('click', () => this.closeAlert());
  }

  closeAlert() {
    this.element.classList.remove('-enabled');
  }

}
