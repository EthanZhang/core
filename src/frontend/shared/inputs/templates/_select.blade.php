<div class="form-group has-value">
    <label class="multi-label" for="{{$attribute_name}}">@bentoLabel($attribute)</label>
    <select @bentoInput($attribute)>
        @if(!$attribute['required'])
            <option value="null"></option>
        @endif
        @foreach(array_get($attribute, 'options', []) as $value => $option)
            <option value="{{$value}}" @if($model->$attribute_name == $value) selected @endif>{{$option}}</option>
        @endforeach
    </select>
    @include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>
