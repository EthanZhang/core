<input type="hidden" name="{{$attribute_name}}" value="0">
<div class="form-group checkbox">
	<label for="{{$attribute_name}}">
		<input
		type="checkbox"
		name="{{$attribute_name}}"
		id="{{$attribute_name}}"
		{{ array_get($model, $attribute_name) ? "checked" : "" }}
		>
		@bentoLabel($attribute)
	</label>
	@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>

<script>
	(function() {
		var input = document.querySelector('input#{{$attribute_name}}');

		input.value = {{ array_get($model, $attribute_name) ? "1" : "0" }};
		input.addEventListener('click', function(e) {
			var numberValue = e.target.value === "1" ? 0 : 1;
			e.target.value = numberValue;
		});

	})();
</script>
