<?php
	$value = array_get($model, $attribute_name);
	$url = asset('storage/' . $value);
?>
<div class="form-group file-input">
	<div class="preview"
		@if (isset($value))
			style="display: block"
		@endif
	>
		@if (isset($value))
			<img src="{{ $url }}">
		@endif
	</div>
	<div class="input">
		<label for="bentobox_{{ $attribute_name }}">@bentoLabel($attribute)</label>
		<input type="file" name="bentobox_{{ $attribute_name }}">
		<button type="button" class="secondary-button">Choose File</button>
		<div class="message">
			@if (isset($value))
				{{ $value }}
			@else
				(No file chosen)
			@endif
		</div>
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
	</div>
</div>
