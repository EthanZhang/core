<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Bentobox</title>

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:300" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="/vendor/bentobox/iconfonts.css">
	<link type="text/css" rel="stylesheet" href="/vendor/bentobox/app.css" />
	<script type="text/javascript" src="/vendor/bentobox/vendor/multiselect/multiselect.min.js"></script>
</head>
<body>
	<div id="main-wrapper">
		@if(!isset($hideSidebar))
			@include('bentobox::core.header._header')
			@include('bentobox::core.sidebar._sidebar')
		@endif

		<main class="container">
			@yield('content')
		</main>
	</div>

	<script type="text/javascript" src="/vendor/bentobox/app.js" async></script>
</body>
</html>
