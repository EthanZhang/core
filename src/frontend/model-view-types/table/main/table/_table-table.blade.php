<style>
	@media only screen and (max-width: 760px) {
		@for ($i = 1; $i <= count($attributes); $i++)
		td:nth-of-type({{$i}}):before { content: "{{ $attributes->get($attributes->keys()[$i - 1])['label'] }}"; }
		@endfor
	}
</style>

<table class="table-table">
	<thead>
		<tr>
			<th class="empty"></th>
			@foreach ($attributes as $attributeName => $attributeValue)
			<th
				data-sort="{{ $attributeName }}"
				class="sortable-header @if ($order['by'] === $attributeName) active @if ($order['direction'] === 'DESC') descending @else ascending @endif @endif"
			>
				<span>{{ array_get($attributeValue, 'label') }}</span>
				<i class="icon-ascending-sorting"></i>
				<i class="icon-descending-sorting"></i>
			</th>
		@endforeach
	</tr>
</thead>
<tbody>
	@foreach ($records as $record)
	<tr>
		<td class="checkbox"><input class="select-row" type="checkbox" data-id="{{$record['id']}}" name="select-{{$record['id']}}"></td>
		@foreach ($attributes as $attributeName => $attributeValue)
			<td data-th="{{ array_get($attributeValue, 'label') }}">
				{!! $record[$attributeName] !!}
			</td>
		@endforeach
	</tr>
	@endforeach
</tbody>
</table>
