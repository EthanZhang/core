export class TableControl {
  constructor (table, control) {
    this.table = table;
    this.control = control;
    this.selectAll = document.querySelector('.select-all');
    this.deleteAction = document.querySelector('.delete-action');
    this.deleteForm = document.querySelector('.delete-form');
    this.checkboxes = Array.prototype.slice.call(table.querySelectorAll('.select-row'));

    this.checkState();
    this.bindSelectAll();
    this.bindDeleteAction();
    this.bindCheckboxes();
  }

  checkState() {
    this.selectAll.checked = this.checkIfCheckboxShouldBeChecked(this.checkboxes)
  }

  checkIfCheckboxShouldBeChecked(checkboxes) {
    const isChecked = checkboxes.findIndex(checkbox => {
      return !checkbox.checked;
    });
    return checkboxes.length > 0 && isChecked === -1;
  }

  bindCheckboxes() {
    this.checkboxes.map(checkbox => {
      checkbox.addEventListener('change', () => this.checkState());
    })
  }

  bindSelectAll() {
    this.selectAll.addEventListener('change', () => {
      this.handleSelectAll(this.checkboxes, this.selectAll.checked);
    });
  }

  handleSelectAll(checkboxes, areAllOn) {
    this.checkboxes.map(checkbox => checkbox.checked = areAllOn);
  }

  bindDeleteAction() {
    this.deleteAction.addEventListener('click', () => {
      this.handleDelete(this.checkboxes, this.deleteForm);
    })
  }

  handleDelete(checkboxes, form) {
    let idsToDelete = [];
    checkboxes.map(checkbox => {
      if (checkbox.checked) idsToDelete.push(+checkbox.dataset.id);
    });

    form.querySelector('#ids_to_delete').value = idsToDelete;
    form.submit();
  }

}
