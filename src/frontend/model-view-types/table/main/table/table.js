import { TableControl } from './table-control';

export class Table {
  constructor(element, control) {
    this.element = element;
    new TableControl(element, control);
    Array.prototype.slice.call(this.element.querySelectorAll('thead tr'))
      .map(header => header.addEventListener('click', e => this.handleHeaderClick(e)));
  }

  handleHeaderClick(e) {
    let params = this.formatQueryParams(location.search);
    const sort = e.target.dataset.sort;
    if (params.orderBy && params.orderBy === sort) {
      if (params.orderDirection && params.orderDirection === 'DESC') {
        params.orderDirection = 'ASC';
      } else {
        params.orderDirection = 'DESC';
      }
    } else {
      params.orderDirection = 'DESC';
      params.orderBy = sort;
    }

    this.assignSearchToNewParams(params);
  }

  formatQueryParams(search) {
    const rawParams = search.substring(1);
    let params = {};
    rawParams.split('&').map(param => {
      const split = param.split('=');
      params[split[0]] = split[1];
    });
    return params;
  }

  assignSearchToNewParams(params) {
    let search = '?';
    let count = 0;
    const length = Object.keys(params).length - 1;

    for (const key in params) {
      if(params.hasOwnProperty(key)) {
        search += key + '=' + params[key];
        if (count < length) {
          search += '&';
        }
        count++;
      }
    }
    location.search = search;
  }
}
